package com.songorder.service;

import com.songorder.pojo.Song;

import java.util.List;
import java.util.Map;

public interface OrderService {
    void ordersong(Map songInfo);

    List<Song> queryDBList();

    void addQMList(String keyword) throws InterruptedException;

    void deleteQMEles(Integer size);

    void deleteQMByid(Integer deleteId);

    void deleteDBByid(Integer DeleteLineIndex);

    void play(String keyword) throws InterruptedException;

    void init();

    Map queryTime();

    void start();

}
