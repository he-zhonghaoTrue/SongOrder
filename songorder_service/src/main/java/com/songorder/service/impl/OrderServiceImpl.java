package com.songorder.service.impl;

import com.songorder.dao.OrderDao;
import com.songorder.service.OrderService;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.songorder.pojo.Song;

import java.util.*;


@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDao orderDao;
    private static ChromeDriver chromeDriver;
    String currentTime;
    String totalTime;
    String currentSong;
    String currentSinger;
    String currentLyrics;

    public void init() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> tabs = new ArrayList<>(chromeDriver.getWindowHandles());
                chromeDriver.switchTo().window(tabs.get(1));

                while (true) {
                    try {
                        currentTime = chromeDriver.findElementByXPath("/html/body/div/div[4]/div[3]/div[1]/div[2]").getText().split(" / ")[0];
                        totalTime = chromeDriver.findElementByXPath("/html/body/div/div[4]/div[2]/div/div[2]/div/div/ul[2]/li[1]/div/div[5]").getText();
                        currentSong = chromeDriver.findElementByCssSelector("#app > div.mod_player > div.player__ft > div.player_music > div.player_music__info > a:nth-child(1)").getText();
                        currentSinger = chromeDriver.findElementByCssSelector("#app > div.mod_player > div.player__ft > div.player_music > div.player_music__info > a.playlist__author").getText();
                        currentLyrics = chromeDriver.findElementByClassName("on").getText();
                        if (totalTime.split(":")[1].equals("00")) {
                            //04:00
                            totalTime = "0" + (Integer.parseInt(totalTime.split(":")[0]) - 1) + ":60";
                        }
//                        System.out.println("currentTime = " + currentTime + " totalTime = " + totalTime);
                        if (totalTime.equals(currentTime)) {
                            System.out.println("播放完毕");
                            deleteQMByid(1);
                            deleteDBByid(0);
                            List<Song> songs = (List<Song>) queryDBList();
                            Collections.reverse(songs);
                            System.out.println("songs = " + songs);
                            if (chromeDriver.findElementByCssSelector("#app > div.mod_player > div.player__bd > div > div.sb_scrollable.sb_main.sb_viewport > div > div > ul.songlist__list").findElements(By.tagName("li")).size() == 0) {
                                for (int i = 0; i < songs.size(); i++) {
                                    Song song = songs.get(i);
                                    String singername = song.getSingername();
                                    String songname = song.getSongname();
                                    String keyword = songname + " " + singername;
                                    if (i == songs.size() - 1) {
                                        play(keyword);
                                    } else {
                                        addQMList(keyword);
                                    }
                                }
                            } else {
                                for (int i = 0; i < songs.size(); i++) {
                                    Song song = songs.get(i);
                                    String singername = song.getSingername();
                                    String songname = song.getSongname();
                                    String keyword = songname + " " + singername;
                                    addQMList(keyword);
                                }
                            }
                        }
                    } catch (Exception e) {
//                        System.out.println("错误！！！ = " + e.getMessage());
                        continue;
                    }
                }
            }
        }).start();
    }

    static {
        System.setProperty("webdriver.chrome.driver", "D:\\develop\\chromeDriver\\chromedriver.exe");
        chromeDriver = new ChromeDriver();
        chromeDriver.get("https://y.qq.com/?ADTAG=myqq#type=index");
    }

    @Override
    public void ordersong(Map songInfo) {
        orderDao.ordersong(songInfo);
        System.out.println("songInfo = " + songInfo);
    }

    @Override
    public List<Song> queryDBList() {
        List<Song> list = orderDao.queryDBList();
        return list;
    }

    @Override
    public void addQMList(String keyword) throws InterruptedException {
        List<String> tabs = new ArrayList<>(chromeDriver.getWindowHandles());
        chromeDriver.switchTo().window(tabs.get(0));
        chromeDriver.findElementByXPath("/html/body/div/div/div[1]/div/div[1]/div[1]/button").click();
        Thread.sleep(1000);
        chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/input").sendKeys(keyword);
        chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/button").click();
        Thread.sleep(1000);
        chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/input").click();
        chromeDriver.findElementByXPath("/html/body/div/div/div[2]").click();
        try {
//            chromeDriver.findElementByXPath("/html/body/div/div/div[3]/div/div/div[4]/ul[2]/li[1]/div/div[2]/span/a/div/span").click();
            chromeDriver.findElementByCssSelector("#app > div > div.main > div > div > div.mod_songlist > ul.songlist__list > li:nth-child(1) > div > div.songlist__songname > span > a > div").click();
            Thread.sleep(1000);
            chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/div/div[3]/a[5]").click();
            chromeDriver.findElementByXPath("/html/body/div[2]/div/div/div/ul/li[1]/a").click();
            chromeDriver.findElementByXPath("/html/body/div[2]/div/div/div/ul/li[1]/div/div/a[1]").click();
        } catch (Exception e) {
//            chromeDriver.findElementByXPath("/html/body/div/div/div[3]/div/div/div[3]/ul[2]/li[1]/div/div[2]/span/a/div").click();
            chromeDriver.findElementByCssSelector("#app > div > div.main > div > div > div.mod_songlist > ul.songlist__list > li:nth-child(1) > div > div.songlist__songname > span > a").click();
            Thread.sleep(1000);
            chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/div/div[3]/a[5]").click();
            chromeDriver.findElementByXPath("/html/body/div[2]/div/div/div/ul/li[1]/a").click();
            chromeDriver.findElementByXPath("/html/body/div[2]/div/div/div/ul/li[1]/div/div/a[1]").click();
        } finally {
            tabs = new ArrayList<>(chromeDriver.getWindowHandles());
            chromeDriver.switchTo().window(tabs.get(1));
        }
    }

    @Override
    public void deleteQMEles(Integer size) {
        List<String> tabs = new ArrayList<>(chromeDriver.getWindowHandles());
        chromeDriver.switchTo().window(tabs.get(1));
        for (int i = 3; i <= size + 1; i++) {
            chromeDriver.findElementByXPath("/html/body/div/div[4]/div[2]/div/div[2]/div/div/ul[2]/li[" + i + "]/div/div[1]/input").click();
        }
        chromeDriver.findElementByXPath("/html/body/div/div[4]/div[2]/div/div[1]/a[4]").click();
    }

    @Override
    public void deleteQMByid(Integer deleteId) {
        List<String> tabs = new ArrayList<>(chromeDriver.getWindowHandles());
        chromeDriver.switchTo().window(tabs.get(1));
        chromeDriver.findElementByXPath("/html/body/div/div[4]/div[2]/div/div[2]/div/div/ul[2]/li[" + deleteId + "]/div/div[1]/input").click();
        chromeDriver.findElementByXPath("/html/body/div/div[4]/div[2]/div/div[1]/a[4]").click();
    }

    @Override
    public void deleteDBByid(Integer DeleteLineIndex) {
        List<Song> songs = orderDao.queryDBList();
        System.out.println("songs = " + songs);
        Integer id = songs.get(DeleteLineIndex).getId();
        System.out.println("id = " + id);
        orderDao.deleteDBByid(id);
    }

    @Override
    public void play(String keyword) throws InterruptedException {
        List<String> tabs = new ArrayList<>(chromeDriver.getWindowHandles());
        chromeDriver.switchTo().window(tabs.get(0));
        chromeDriver.findElementByXPath("/html/body/div/div/div[1]/div/div[1]/div[1]/button").click();
        Thread.sleep(1000);
        chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/input").sendKeys(keyword);
        chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/button").click();
        Thread.sleep(1000);
        chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/input").click();
        chromeDriver.findElementByXPath("/html/body/div/div/div[2]").click();
        try {
            chromeDriver.findElementByCssSelector("#app > div > div.main > div > div > div.mod_songlist > ul.songlist__list > li:nth-child(1) > div > div.songlist__songname > span > a> div").click();
            //#app > div > div.main > div > div > div.mod_songlist > ul.songlist__list > li:nth-child(1) > div > div.songlist__songname > span > a
            Thread.sleep(1000);
            chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/div/div[3]/a[1]").click();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            chromeDriver.findElementByCssSelector("#app > div > div.main > div > div > div.mod_songlist > ul.songlist__list > li:nth-child(1) > div > div.songlist__songname > span > a > div").click();
            Thread.sleep(1000);
            chromeDriver.findElementByXPath("/html/body/div/div/div[2]/div[1]/div/div[3]/a[1]").click();
        } finally {
            tabs = new ArrayList<>(chromeDriver.getWindowHandles());
            chromeDriver.switchTo().window(tabs.get(1));
        }
    }

    @Override
    public Map queryTime() {
        Map<Object, Object> time = new HashMap<>();
        Integer c_M = Integer.parseInt(currentTime.split(":")[0]);
        Float c_S = Float.parseFloat("0." + currentTime.split(":")[1]);
        Integer t_M = Integer.parseInt(totalTime.split(":")[0]);
        Float t_S = Float.parseFloat("0." + totalTime.split(":")[1]);
        Float c = c_M + c_S;
        Float t = t_M + t_S;
        time.put("currentTime", c);
        time.put("totalTime", t);
        time.put("currentSong", currentSong);
        time.put("currentSinger", currentSinger);
        time.put("currentLyrics", currentLyrics);
        System.out.println("time = " + time);
        return time;
    }

    @Override
    public void start() {
        List<String> tabs = new ArrayList<>(chromeDriver.getWindowHandles());
        chromeDriver.switchTo().window(tabs.get(1));
        chromeDriver.findElementByCssSelector("#app > div.mod_player > div.player__ft > a.btn_big_play").click();
    }

    @Test
    public void test() {
        System.out.println("orderDao = " + orderDao);
    }
}
