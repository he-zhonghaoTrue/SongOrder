# SSM+selenium的使用QQ音乐网页版点歌服务

#### 介绍
SSM+selenium的使用QQ音乐网页版点歌服务

#### 软件架构
SSM+selenium


#### 使用说明

1.  下载对应版本谷歌浏览器的java selenium驱动程序，并在程序修改相应代码
2.  请使用提供的sql脚本恢复数据库
3.  依赖ssm框架和maven，请确保环境支持

