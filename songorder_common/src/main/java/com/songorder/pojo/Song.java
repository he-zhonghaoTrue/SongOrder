package com.songorder.pojo;

public class Song {
    private String songname;
    private String singername;
    private Integer id;

    @Override
    public String toString() {
        return "Song{" +
                "songname='" + songname + '\'' +
                ", singername='" + singername + '\'' +
                ", id=" + id +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSingername() {
        return singername;
    }

    public void setSingername(String singername) {
        this.singername = singername;
    }

    public String getSongname() {
        return songname;
    }

    public void setSongname(String songname) {
        this.songname = songname;
    }
}
