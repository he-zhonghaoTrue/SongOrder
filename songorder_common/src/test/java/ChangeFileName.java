import java.io.File;

public class ChangeFileName {

    public static void recursiveTraversalFolder(String path) {
        File file = new File(path);
        String[] list = file.list();
        for (String oldName : list) {
            String[] names = oldName.split("：");
            String newName = names[1];
            File oldFile = new File(path + "\\" + oldName);
            System.out.println("oldFile = " + oldFile);
            File newFile = new File(path + "\\" + newName);
            System.out.println("newFile = " + newFile);
            oldFile.renameTo(newFile);
            System.out.println();
        }
    }

    public static void main(String[] args) {
        for (int i = 79; i <= 99; i++) {
            recursiveTraversalFolder("E:\\1、后端\\ASSG\\6.互联网高级技术&大型企业级项目-第79-99天\\day" + i);
            System.out.println();
        }
    }
}

