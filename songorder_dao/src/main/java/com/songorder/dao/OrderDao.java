package com.songorder.dao;

import org.springframework.stereotype.Repository;
import com.songorder.pojo.Song;

import java.util.List;
import java.util.Map;

@Repository
public interface OrderDao {
    void ordersong(Map songInfo);

    List<Song> queryDBList();

    void deleteDBByid(Integer id);
}
