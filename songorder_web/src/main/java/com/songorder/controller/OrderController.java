package com.songorder.controller;

import com.songorder.entity.Result;
import com.songorder.service.OrderService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.songorder.pojo.Song;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ordercontroller")

public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping("/ordersong.do")
    public void ordersong(@RequestBody Map songInfo) throws InterruptedException {
        List list = orderService.queryDBList();
        System.out.println("list = " + list);
        if (list.size() == 0) {
            System.out.println("播放列表为空！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！");
            orderService.ordersong(songInfo);
            addQMLiist();
        } else {
            try{
                orderService.deleteQMEles(list.size() - 1);
            }catch (Exception e){
                System.out.println("删除QM播放列表出错 = " + e.getMessage());
            }
            orderService.ordersong(songInfo);
        }
//        return new Result(true, "请求成功");
    }

    @RequestMapping("/queryDBList.do")
    public Result queryDBList() {
        List list = orderService.queryDBList();
        return new Result(true, "查询歌单成功", list);
    }

    @RequestMapping("/addQMList.do")
    public void addQMLiist() throws InterruptedException {
        List<Song> songs = (List<Song>) queryDBList().getData();
        Collections.reverse(songs);
        System.out.println("songs = " + songs);
        for (int i = 0; i < songs.size(); i++) {
            Song song = songs.get(i);
            String singername = song.getSingername();
            String songname = song.getSongname();
            String keyword = songname + " " + singername;
            if (i == songs.size() - 1) {
                orderService.play(keyword);
            } else {
                orderService.addQMList(keyword);
            }
        }
    }

    @RequestMapping("/addRecentQMList.do")
    public void addRecentQMList(Map songInfo) throws InterruptedException {
        String singername = (String) songInfo.get("singername");
        String songname = (String) songInfo.get("songname");
        String keyword = songname + " " + singername;
        System.out.println("keyword = " + keyword);
        orderService.addQMList(keyword);
    }

    @RequestMapping("/deleteByid.do")
    public void deleteByid(@RequestBody Map param) {
        //param = {beforeDeleteLineCount=10, DeleteLineIndex=1}
        Integer beforeDeleteLineCount = (Integer) param.get("beforeDeleteLineCount");
        Integer DeleteLineIndex = (Integer) param.get("DeleteLineIndex");
        Integer DeleteId = beforeDeleteLineCount - DeleteLineIndex;
        System.out.println("beforeDeleteLineCount = " + beforeDeleteLineCount);
        System.out.println("DeleteLineIndex = " + DeleteLineIndex);
        System.out.println("DeleteId = " + DeleteId);
        try {
            orderService.deleteQMByid(DeleteLineIndex + 1);
        } catch (Exception e) {
            System.out.println("e.getMessage() = " + e.getMessage());
        }

        orderService.deleteDBByid(DeleteLineIndex);
    }

    @RequestMapping("/init.do")
    public void init() {
        orderService.init();
    }

    @RequestMapping("queryTime.do")
    public Map queryTime() {
        return orderService.queryTime();
    }

    @RequestMapping("start.do")
    public void start(){
        orderService.start();
    }
}
